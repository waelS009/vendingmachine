package com.vendermachine.assignment.controller;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import com.vendermachine.assignment.dto.PurshaseDetails;
import com.vendermachine.assignment.enumeration.Coin;
import com.vendermachine.assignment.services.IAccountService;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@ContextConfiguration(classes = {AccountController.class})
@ExtendWith(SpringExtension.class)
class AccountControllerTest {
    @Autowired
    private AccountController accountController;

    @MockBean
    private IAccountService iAccountService;

    @Test
    void testAddCoin() throws Exception {
        doNothing().when(this.iAccountService).addCoin((java.security.Principal) any(), (Coin) any());
        MockHttpServletRequestBuilder getResult = MockMvcRequestBuilders.get("/deposit");
        MockHttpServletRequestBuilder requestBuilder = getResult.param("coin", String.valueOf(Coin.FIVE_CENT));
        MockMvcBuilders.standaloneSetup(this.accountController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void testBuyProduct() throws Exception {
        PurshaseDetails purshaseDetails = new PurshaseDetails();
        purshaseDetails.setChange(new ArrayList<>());
        purshaseDetails.setProducts(new ArrayList<>());
        purshaseDetails.setTotal(10.0);
        when(this.iAccountService.buyProduct((java.security.Principal) any(), (Long) any(), (Integer) any()))
                .thenReturn(purshaseDetails);
        MockHttpServletRequestBuilder getResult = MockMvcRequestBuilders.get("/buy");
        MockHttpServletRequestBuilder paramResult = getResult.param("amount", String.valueOf(1));
        MockHttpServletRequestBuilder requestBuilder = paramResult.param("productId", String.valueOf(1L));
        MockMvcBuilders.standaloneSetup(this.accountController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content().string("{\"total\":10.0,\"products\":[],\"change\":[]}"));
    }

    @Test
    void testReset() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/reset");
        MockMvcBuilders.standaloneSetup(this.accountController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void testReset2() throws Exception {
        MockHttpServletRequestBuilder getResult = MockMvcRequestBuilders.get("/reset");
        getResult.characterEncoding("Encoding");
        MockMvcBuilders.standaloneSetup(this.accountController)
                .build()
                .perform(getResult)
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}

