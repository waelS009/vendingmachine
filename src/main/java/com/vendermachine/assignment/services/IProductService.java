package com.vendermachine.assignment.services;

import com.vendermachine.assignment.domain.Product;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.security.Principal;
import java.util.List;

public interface IProductService{
    public Boolean checkProduct(Long productId);
    public void deleteProduct(Principal principal, Long id);
    public void updateProduct(Principal principal, Product product);
    public void addProduct(Principal principal, Product product);
    public List<Product> getProducts();

}
