package com.vendermachine.assignment.services.emailSender;

public interface IEmailSender {
    public void sendEmail(String to,String email);
}
