package com.vendermachine.assignment.services;

import com.vendermachine.assignment.domain.Product;
import com.vendermachine.assignment.domain.User;
import com.vendermachine.assignment.repositories.AppUserRepository;
import com.vendermachine.assignment.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;

@Service
public class ProductServiceImpl implements IProductService{

    @Autowired
    private AppUserRepository userRepository;

    @Autowired
    private ProductRepository productRepository;

    @Override
    public List<Product> getProducts() {
        return (List) productRepository.findAll();
    }


    @Override
    public void updateProduct(Principal principal,Product product) {
        productRepository.save(product);
    }

    @Override
    public void addProduct(Principal principal,Product product) {
        User user = userRepository.findByUsername(principal.getName()).get();
        product.setSeller(user);
        productRepository.save(product);
    }

    @Override
    public void deleteProduct(Principal principal,Long id) {
        productRepository.deleteById(id);
    }


    @Override
    public Boolean checkProduct(Long productId) {
        return productRepository.existsById(productId);
    }

    private boolean isProductOwner(Principal principal, Product product){
        return userRepository.findByUsername(principal.getName()).get().getId().equals(product.getId());
    }

}
