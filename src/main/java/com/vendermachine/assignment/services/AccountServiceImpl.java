package com.vendermachine.assignment.services;

import com.vendermachine.assignment.domain.Product;
import com.vendermachine.assignment.domain.User;
import com.vendermachine.assignment.dto.PurshaseDetails;
import com.vendermachine.assignment.enumeration.Coin;
import com.vendermachine.assignment.repositories.AppUserRepository;
import com.vendermachine.assignment.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.util.List;

@Service
public class AccountServiceImpl implements IAccountService {

    @Autowired
    private AppUserRepository userRepository;

    @Autowired
    private ProductRepository productRepository;

    @Override
    public void addCoin(Principal principal, Coin coin) {
        User currentUser =  userRepository.findByUsername(principal.getName()).get();
        switch (coin){
            case FIVE_CENT  : {
                currentUser.setDeposit(currentUser.getDeposit() + 5);
                break;
            }
            case TEN_CENT  : {
                currentUser.setDeposit(currentUser.getDeposit() + 10);
            }
            case TWENTY_CENT  : {
                currentUser.setDeposit(currentUser.getDeposit() + 20);
            }
            case FIFTY_CENT  : {
                currentUser.setDeposit(currentUser.getDeposit() + 50);
            }
            case HUNDRED_CENT : {
                currentUser.setDeposit(currentUser.getDeposit() + 100);
            }
            default: throw new IllegalArgumentException("COIN NOT ACCEPTED");
        }
        currentUser.setDeposit(0D);
        userRepository.save(currentUser);
    }

    @Override
    public PurshaseDetails buyProduct(Principal principal, Long productId, Integer amount) {
        User currentUser =  userRepository.findByUsername(principal.getName()).get();
        Double currentDeposit = currentUser.getDeposit();
        Product product = productRepository.findById(productId).get();
        if(product.getAmountAvailable()<amount || currentDeposit < amount*product.getCoast()){
            throw new IllegalArgumentException("aount not available");
        }
        product.setAmountAvailable(product.getAmountAvailable()-amount);
        productRepository.save(product);
        Double price = amount*product.getCoast();
        PurshaseDetails purshaseDetails = new PurshaseDetails();
        purshaseDetails.setTotal(amount*product.getCoast());
        purshaseDetails.getProducts().add(product);
        boolean canDivide = true;

        while (canDivide){
            if(price % 100 ==0){
                purshaseDetails.getChange().add(Coin.HUNDRED_CENT);
            }else if(price % 50 ==0){
                purshaseDetails.getChange().add(Coin.FIFTY_CENT);
            }else if(price % 20 ==0){
                purshaseDetails.getChange().add(Coin.TWENTY_CENT);
            }else if(price % 10 ==0){
                purshaseDetails.getChange().add(Coin.TEN_CENT);
            }else if(price % 5 ==0){
                purshaseDetails.getChange().add(Coin.FIVE_CENT);
            }else{
                canDivide = false;
            }
        }
        return purshaseDetails;
    }

    @Override
    public void reset(Principal principal) {
        User currentUser =  userRepository.findByUsername(principal.getName()).get();
        currentUser.setDeposit(0D);
        userRepository.save(currentUser);
    }
}
