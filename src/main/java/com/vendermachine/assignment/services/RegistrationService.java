package com.vendermachine.assignment.services;

import com.vendermachine.assignment.domain.User;
import com.vendermachine.assignment.dto.RegistrationRequest;
import com.vendermachine.assignment.enumeration.AppUserRole;
import com.vendermachine.assignment.services.emailSender.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RegistrationService {

    @Autowired
    private EmailValidator emailValidator;

    @Autowired
    private AppUserService appUserService;

    @Autowired
    private EmailService emailService;


    public void register(RegistrationRequest request){
       appUserService.signUpUser(
                new User(request.getUsername(),
                        request.getPassword(),
                        AppUserRole.BUYER)
        );
    }

}
