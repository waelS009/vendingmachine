package com.vendermachine.assignment.services;

import com.vendermachine.assignment.domain.Product;
import com.vendermachine.assignment.dto.PurshaseDetails;
import com.vendermachine.assignment.enumeration.Coin;
import org.springframework.web.bind.annotation.RequestBody;

import java.security.Principal;
import java.util.List;

public interface IAccountService {
    public void addCoin(Principal principal, Coin coin);
    public PurshaseDetails buyProduct(Principal principal, Long productId, Integer amount);
    public void reset(Principal principal);
}
