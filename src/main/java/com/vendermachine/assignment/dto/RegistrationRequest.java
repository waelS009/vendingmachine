package com.vendermachine.assignment.dto;


import com.vendermachine.assignment.enumeration.AppUserRole;

public class RegistrationRequest {
    private final String username;
    private final String password;
    private AppUserRole role;

    public RegistrationRequest(String username, String password, AppUserRole role) {
        this.username = username;
        this.password = password;
        this.role = role;
    }


    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }

    public AppUserRole getRole() {
        return role;
    }

    public void setRole(AppUserRole role) {
        this.role = role;
    }
}
