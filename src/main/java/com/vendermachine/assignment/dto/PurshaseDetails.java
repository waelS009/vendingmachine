package com.vendermachine.assignment.dto;

import com.vendermachine.assignment.domain.Product;
import com.vendermachine.assignment.enumeration.Coin;

import java.util.ArrayList;
import java.util.List;

public class PurshaseDetails {

    private Double total;
    private List<Product> products;
    private List<Coin> change;

    public PurshaseDetails(){
        this.products = new ArrayList<>();
        this.change = new ArrayList<>();
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public List<Coin> getChange() {
        return change;
    }

    public void setChange(List<Coin> change) {
        this.change = change;
    }
}
