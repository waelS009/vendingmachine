package com.vendermachine.assignment.controller;

import com.vendermachine.assignment.dto.RegistrationRequest;
import com.vendermachine.assignment.services.RegistrationService;
import com.vendermachine.assignment.services.StorageFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping
public class AppUserController {

    @Autowired
    private RegistrationService registrationService;


    @RequestMapping(value="/user",method = RequestMethod.POST)
    public void register(@RequestBody RegistrationRequest request){
         registrationService.register(request);
    }

}
