package com.vendermachine.assignment.controller;

import com.vendermachine.assignment.dto.PurshaseDetails;
import com.vendermachine.assignment.enumeration.Coin;
import com.vendermachine.assignment.services.IAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
public class AccountController {

    @Autowired
    private IAccountService accountServiceImpl;

    @RequestMapping(value ="/deposit",method = RequestMethod.GET)
    @PreAuthorize("hasRole('BUYER')")
    public ResponseEntity addCoin(Principal principal, @RequestParam(name = "coin", required = true)Coin coin){
            accountServiceImpl.addCoin(principal,coin);
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/buy",method = RequestMethod.GET )
    @PreAuthorize("hasRole('BUYER')")
    public ResponseEntity<PurshaseDetails> buyProduct(Principal principal, @RequestParam(name = "productId", required = true) Long productId , @RequestParam(name = "amount", required = true)Integer amount){
        return new ResponseEntity<>(accountServiceImpl.buyProduct(principal,productId,amount), HttpStatus.OK);
    }

//    Implement /reset endpoint so users with a "buyer" role can reset their deposit back to 0
//            • Take time to think about possible edge cases and access issues that should be solved

    @RequestMapping(value ="/reset", method = RequestMethod.GET)
    @PreAuthorize("hasRole('BUYER')")
    public ResponseEntity<PurshaseDetails> reset(Principal principal){
        return new ResponseEntity<>(null, HttpStatus.OK);
    }
}
