package com.vendermachine.assignment.controller;

import com.vendermachine.assignment.domain.Product;
import com.vendermachine.assignment.services.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.management.relation.Role;
import java.security.Principal;
import java.util.List;

@RestController
public class ProductController {

    @Autowired
    private IProductService productServiceImpl;

    @RequestMapping(value = "/products",method = RequestMethod.GET )
    public ResponseEntity<List<Product>> getProducts(){
        return new ResponseEntity<List<Product>>(productServiceImpl.getProducts(), HttpStatus.OK);
    }

    @RequestMapping(value = "/products",method = RequestMethod.POST )
    @PreAuthorize("hasRole('SELLER')")
    public void addProduct(Principal principal,@RequestBody Product product){
        productServiceImpl.addProduct(principal,product);
    }

    @RequestMapping(value = "/products",method = RequestMethod.PUT )
    @PreAuthorize("hasRole('SELLER')")
    public void updateProduct(Principal principal, @RequestBody Product product){
        productServiceImpl.updateProduct(principal,product);
    }

    @RequestMapping(value = "/products/{id}",method = RequestMethod.DELETE )
    @PreAuthorize("hasRole('SELLER')")
    public void deleteProduct(Principal principal, @PathVariable Long id){

        productServiceImpl.deleteProduct(principal,id);
    }
}
