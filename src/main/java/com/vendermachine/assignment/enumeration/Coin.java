package com.vendermachine.assignment.enumeration;

public enum Coin {
    FIVE_CENT,TEN_CENT,TWENTY_CENT,FIFTY_CENT,HUNDRED_CENT
}
